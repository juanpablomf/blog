+++
title = "Combinatoria: como contar en problemas de computación"
author = ["Juan Pablo Morales"]
date = 2019-01-10T16:28:00-03:00
draft = false
+++

En este articulo me gustaría mostrarles algunas técnicas de base de la matemática del conteo -comúnmente llamada Combinatoria-
que me parecen útiles e incluso esenciales para cualquier programador.


## Estimaciones de tiempo y conteo {#estimaciones-de-tiempo-y-conteo}

En computación uno se encuentro mas temprano que tarde con la necesidad de saber contar la cantidad de objetos que tienen cierta estructura.

A modo de ejemplo hace algunos meses me tope con el problema de programación competitiva siguiente.

> Defina un método **max\_product(A)**, que para una lista de flotantes A de
> largo N entregue el máximo producto consecutivo de elementos de la lista.

Si tenemos una lista llamaremos **slice** de la lista cualquier sublista cuyos elementos sean consecutivos en la lista original.
Por ejemplo para la lista \\([-1,3,4,-7,1,-18]\\), \\([-1,3,4]\\) es un slice, en cambio \\([-1,4]\\) no es un slice.

Una primera idea para resolver el problema por fuerza bruta es:

-   Generar todos los slices de la lista A.
-   Calcular el producto de todos los elementos para cada slice.
-   Encontrar el máximo de los productos.

Este es el look del algoritmo de fuerza bruta en Python.

```python
def generate_all_slices(A):
    for i in range(len(A)):
        for j in range(i,len(A)):
            yield A[i:j+1]

def prod(A):
    if len(A) == 0:
        return 1
    else:
        return A[0] * prod(A[1:])

def solution(A):
    list_of_slices = list(generate_all_slices(A))
    m = prod(list_of_slices[0])
    for l in list_of_slices:
        m = max(m, prod(l))
    return m
```

Hasta aquí todo bien, el algoritmo es correcto. Pero que tan rápido es?

Podemos descomponer _el tiempo tomado por la función solution_ de la forma siguiente:

-   El _for loop_ llama exactamente una vez la función prod por cada slice con índices consecutivos de A.
-   La _funcion prod_ se demora proporcionalmente al largo de cada slice. El largo promedio de un slice es \\(\frac{largo(A)}{2}\\)

Por ende el tiempo de ejecución de la solución va a ser \\[ T=\frac{largo(A)}{2}\*slices(A) \\]

_En conclusión: Para saber si nuestra solución es valida debemos encontrar una forma de contar la cantidad de slices._

{{< figure src="/ox-hugo/21136rNW.png" >}}


## Principios fundamentales {#principios-fundamentales}

Tres conceptos intuitivos permiten hacer una gran variedad de conteos. Si bien no son difíciles de entender el arte esta en saber combinarlos y aplicarlos de forma ordenada.

{{< figure src="/ox-hugo/ejemplo-sets.png" >}}


### Principio Aditivo {#principio-aditivo}

El primer principio es probablemente el más fácil de entender:

{{< figure src="/ox-hugo/principio-aditivo.png" >}}


### Principio multiplicativo {#principio-multiplicativo}

{{< figure src="/ox-hugo/principio-multiplicativo.png" >}}

Solo para convencernos un poco más de la validez de este concepto mirémoslo en acción en python:

```python
from itertools import product

def count_tuples(A,B):
    all_tuples = list(product(A,B))
    return len(all_tuples)

count_tuples(range(3),range(5))
```

```text
None
```

Por ultimo, el principio multiplicativo también funciona para tuplas de largo arbitrario:

{{< figure src="/ox-hugo/principio-multiplicativo-generalizado.png" >}}


### Correspondencias {#correspondencias}

{{< figure src="/ox-hugo/correspondencias.png" >}}

Desde un punto de vista computacional uno puede pensar en una correspondencia uno a uno pensando en un diccionario en python:

```python
d = {'norte': 'fuego',
     'sur': 'agua',
     'este': 'tierra',
     'oeste': 'aire'}
```

Las llaves y valores están en correspondencia 1 a 1, por lo tanto hay la misma cantidad: 4 puntos cardinales, 4 elementos naturales.

Lo interesante de esta propiedad es que para contar ciertos objetos, si encontramos una correspondencia uno a uno con otra clase de objetos, podemos contar la segunda clase de objetos y estaremos
resolviendo el problema para la primera clase.

_Esto es muy útil ya que hay ciertas estructuras que son mas fáciles de contar!_


## Aplicaciones {#aplicaciones}


### Caminos completos de un árbol perfecto {#caminos-completos-de-un-árbol-perfecto}

_Para hablar de forma mas simple de los arboles, nos imaginaremos cada árbol como si fuera un árbol genealógico. Con esto se nos hará mucho mas fácil
hablar de cada una de las propiedades del árbol, usando conceptos como hijos, padre, ancestro, etc._

**Problema**

Un camino completo de un árbol es un camino que comienza desde la raíz del árbol y que llega hasta las personas de ultima generación.
Queremos contar la cantidad de este tipo de caminos para _los arboles perfectos_.

Para entender mejor lo que es un arabo perfecto un ejemplo seria el siguiente:

{{< figure src="/ox-hugo/tree2.png" >}}

Lo que distingue a un árbol perfecto de los otros arboles es que _todas las personas de la misma generación tienen la misma cantidad de hijos_.

En el caso del ejemplo:

-   La persona de primera generación tiene 3 hijos
-   Las personas de la segunda generación tienen 2 hijos
-   Las personas de tercera generación tienen 3 hijos.

**Conteo**

La primera buena idea es etiquetar las personas del árbol de forma inteligente: cada generación \\(i\\) del árbol la anotamos con números entre \\(1\\) y la cantidad de hijos que tiene esa generación.

{{< figure src="/ox-hugo/tree-numbered.png" >}}

Ahora nos podemos dar cuenta que _si seguimos un camino completo y leemos los números que aparecen en este camino nos encontramos con una tupla \\((a\_1,a\_2,a\_3)\\)_.

{{< figure src="/ox-hugo/tree-numbered-colored.png" >}}

Por ejemplo para el camino en rojo leemos la tupla de números \\((1,2,3)\\).

Para cada camino completo hay exactamente una tupla. Por lo tanto _hemos construido una correspondencia uno a uno_ entre los caminos completos y las tuplas de la forma siguiente:

\\[
(a\_1,a\_2,a\_3) \, \textrm{donde} \, a\_1 \in \\{1,2,3\\} \, a\_2 \in \\{1,2,3,4,5\\} \, a\_3 \in \\{1,2\\}
\\]

Al tener una correspondencia uno a uno, sabemos que _hay la misma cantidad de caminos que de tuplas_. Contar la cantidad de tuplas es algo muy fácil gracias al principio multiplicativo.

El principio multiplicativo nos dice que hay que multiplicar la cantidad de elementos que puede tomar \\(a\_1\\), \\(a\_2\\) y \\(a\_3\\).

Por lo tanto hay \\(3 \times 2 \times 3 = 18\\) individuos de ultima generación.

Esta prueba es totalmente valida si hubiésemos tomado un árbol de mayor profundidad, o diferentes cantidades de hijos por generación.

{{< figure src="/ox-hugo/arboles-perfectos.png" >}}


### Ordenes de N elementos {#ordenes-de-n-elementos}

**Problema**

Supongamos que tengo \\(N\\) objetos diferentes, como por ejemplo \\(N\\) cartas. De cuantas formas las puedo ordenar?

{{< figure src="/ox-hugo/cards.png" >}}

**Conteo**

La demostracion al igual que para el caso anterior la haremos de forma visual sobre un caso concreto,
pero la demostracion sera aplicable para cualquier numero \\(N\\) de cartas.

La idea inteligente en este caso es construir un arbol perfecto donde aparezcan estas cartas, y luego simplemente utilizar la formula que calculamos para los arboles.
El árbol que construiremos para contar los ordenes de 5 cartas tiene:

-   5 hijos en la primera generación
-   4 hijos en la segunda
-   3 en la tercera
-   2 en la cuarta
-   1 en la ultima

Esto nos da un árbol un tanto grande para visualizar por lo que en las siguientes imágenes mostraremos solo una rama del árbol completa.

{{< figure src="/ox-hugo/card-tree.png" >}}

En este árbol colocamos primero todas las 5 cartas. Esta carta en el primer nivel del árbol se interpretara como la carta que va en primer lugar del orden correspondiente.

Luego como hijos de las primeras cartas que colocamos, pondremos las cartas que pueden ir en segundo lugar del orden sabiendo que la primera carta del orden ya fue elegida y corresponde a la carta que aparece en el padre.
Tenemos 4 opciones ya que un padre fue elegido (lo que nos da \\(5-1\\) opciones).

Para los hijos siguientes hacemos lo mismo, los hijos son las cartas que pueden ir en el i-esimo lugar del orden sabiendo que los ancestros ya fueron utilizados. Con esto tendremos 5 cartas en la primera generación, luego 4, luego 3, etc.

{{< figure src="/ox-hugo/example-tree-cards.png" >}}

Esta claro que con esto podemos rellenar el arbol, y también que a cada camino del arbol corresponde exactamente un orden de cartas. Tenemos una correspondencia entre los caminos completos del arbol y los ordenes de 5 cartas.

Con esto concluimos que las cantidad de formas de ordenar 5 cartas es \\(5 \times 4 \times 3 \times 2 \times 1 = 120\\).

Si hacemos la misma construcción para \\(N\\) cartas distinas tenemos la formula general:

{{< figure src="/ox-hugo/ordenes.png" >}}


### Ordenes parciales y combinaciones {#ordenes-parciales-y-combinaciones}

**Ordenes parciales**

Cuantas formas de elegir \\(k\\) elementos ordenados dentro de \\(N\\) opciones hay?

El árbol por construir es muy similar al árbol anterior solo que esta vez nos detenemos en la k-esima generación en vez de seguir hasta tener \\(N\\) generaciones.

Para la primera generación tenemos \\(N\\) opciones. Para la segunda \\(N-1\\), y así hasta llegar a la generación \\(k\\) donde tenemos \\(N-(k-1)\\). Por lo tanto tenemos la formula siguiente para
los ordenes parciales:

{{< figure src="/ox-hugo/ordenes-parciales.png" >}}

**Combinaciones**

Con este ultimo conteo estaremos llegando a una de las ultimas formulas mas clásicas de la combinatoria.
Cuando queremos contar la forma de extraer \\(k\\) elementos dentro de \\(N\\) pero el orden no nos importa hablaremos de combinaciones en vez de ordenes parciales.
Por ejemplo si quisiera saber la cantidad de manos de 5 cartas que puedo extraer de un mazo de 42 cartas no me importa realmente el orden en el
que aparecen sino que solo me importan las manos que aparecen en mi mano.

Vamos por el calculo, que esta vez sera un poco mas difícil que los anteriores.

Escribamos \\(C(N,k)\\) la cantidad de combinaciones de \\(k\\) elementos dentro de \\(N\\). Construyamos el siguiente árbol:

-   Los elementos de primera generación serán exactamente \\(C(N,k)\\). Cada uno de estos corresponde a \\(k\\) elementos elegido dentro de \\(N\\) (una de las combinaciones).
-   A cada elemento de primera generación le agregamos \\(k \times (k-1) \times \cdots \times 1\\) hijos. Estos hijos corresponden a tomar los elementos que aparecen en el padre y darles un orden (sabemos que hay la cantidad mencionada gracias a nuestro calculo de los ordenes).

Ocupando nuestro calculo de los arboles perfectos sabemos que este arbol tiene exactamente \\(C(N,k) \times \left[k \times (k-1) \times \cdots \times 1\right]\\).

Miremos mas de cerca el árbol que construimos. Si seguimos un camino tenemos al final de este un orden de \\(k\\) elementos. Osea que al final de este camino tenemos un orden parcial de k elementos!
Los tenemos todos? Si, ya que si elegimos un orden parcial cualquiera, este aparecerá en la rama donde los elementos de una combinación sean los mismo elementos que los del orden parcial.

Concluimos entonces que el árbol tiene la misma cantidad de caminos que ordenes parciales de k elementos. Por lo tanto:

\\(C(N,k) \times \left[k \times (k-1) \times \cdots \times 1\right] = N \times (N-1) \times \cdots \times (N-(k-1))\\)

y si despejamos la ecuacion:

\\(C(N,k) = \frac{N \times (N-1) \times \cdots \times (N-(k-1))}{k \times (k-1) \times \cdots \times 1}\\)

Esta ecuación se ve un tanto pesada escrita de esta forma. Si escribimos \\(k \times (k-1) \times \cdots \times 1 = k!\\) luego nos queda la formula siguiente.

{{< figure src="/ox-hugo/combinaciones.png" >}}


## Resolviendo el conteo computacional {#resolviendo-el-conteo-computacional}

Con esto ya tenemos suficientes armas para poder atacar el problema de conteo de slices.

Tomemos una lista y consideremos el espacio entre cada numero de la lista. Agreguemos una caja vacía para cada uno de estos espacios.

{{< figure src="/ox-hugo/slots.png" >}}

La primera buena idea es colocar cajas adicionales labelizadas \\(P\_1,P\_2,\cdots,P\_{n+1}\\) que corresponden
a todos los espacios donde puede iniciar y terminar el slice que estamos construyendo.

{{< figure src="/ox-hugo/slots-sticks-pipe.png" >}}

Por ejemplo si elijo las posiciones \\(P\_2\\) y \\(P\_5\\) estaré eligiendo el slice \\([3,4,-7]\\).

{{< figure src="/ox-hugo/slots-sticks-pipe-chosen.png" >}}

Claramente por cada elección de dos posiciones P obtengo 1 solo slice, y por cada slice existe un solo par
de posiciones P que generan el slice. _Tenemos por lo tanto una relación 1 a 1 entre las formas de elegir 2 posiciones,
dentro de N+1 posiciones posibles]_ (recuerden que en la parte anterior rellenamos con las cajas \\(P\_1,P\_2,\cdots,P\_{n+1}\\) que
son exactamente \\(N+1\\) elementos).-

En las 2 posiciones que estoy buscando para dar inicio y fin al slice, y asi construir la correspondencia uno a uno, _no me importa el orden_. El slice (P2,P5) y el slice (P5,P2) contienen exactamente lo mismo, entonces para no contarlo dos veces tengo que usar las
combinaciones y no los ordenes!

Utilizando la formula de las combinaciones tenemos que la cantidad de slices es \\(C(N+1,2) = \frac{(N+1) \times N}{2}\\)

{{< figure src="/ox-hugo/slices.png" >}}

Esta cantidad es \\(\mathcal{O}(n ^ 2)\\). Según la aproximación que dimos al inicio del articulo \\[T(N) \sim N \times N^2 \sim N^3\\]

En todo nuestro algoritmo de fuerza bruta tiene una complejidad cubica. Esto puede ser aceptable si trabajamos con valores pequeños de \\(N\\),
sin embargo para valores mas grandes tendremos que buscar un algoritmo mas eficiente.
A
Una solución eficiente para el problema es la siguiente:

```python
def maxProduct(l):
    localMax = l[0]
    localMin = l[0]
    maxProd = l[0]
    for i in range(1,len(l)):
        if(l[i] > 0):
            localMax = max(localMax * l[i], l[i])
            localMin = min(localMin * l[i], l[i])
        else:
            localMaxNeg = max(localMin * l[i],l[i])
            localMin = min(localMax * l[i], l[i])
            localMax = localMaxNeg

       maxProd = max(maxProd, localMax)

   return maxProd
```

Esta solución es altamente eficiente \\(\mathcal{O}(n)\\) (!!) sin embargo para entenderla tendremos que verla en otro articulo que se centrara en las demostraciones por inducción.